<!-- ============== Banner sections ============== -->

<div class="banner-structure">
<ul class="banner_slider">
<li><img src="<?php echo base_url() ?>assets/images/banner_1.jpg" alt="banner" />
<div class="text-banner">
<h2 class="roboto">Boost up your local business
<span>Lorem Ipsum is simply dummy text of the printing and typesetting</span></h2> 
</div>
</li>
<li><img src="<?php echo base_url() ?>assets/images/banner_2.jpg" alt="banner" />
<div class="text-banner">
<h2 class="roboto">Boost up your local business
<span>Lorem Ipsum is simply dummy text of the printing and typesetting</span></h2> 
</div>
</li>
<li><img src="<?php echo base_url() ?>assets/images/banner_3.jpg" alt="banner" />
<div class="text-banner">
<h2 class="roboto">Boost up your local business
<span>Lorem Ipsum is simply dummy text of the printing and typesetting</span></h2> 
</div>
</li>
</ul>


<div id="bx-pager">
<a data-slide-index="0" href="#"><img src="<?php echo base_url() ?>assets/images/icons/banner-thumb1.png" alt="banner-thumb" /></a>
<a data-slide-index="1" href="#"><img src="<?php echo base_url() ?>assets/images/icons/banner-thumb2.png" alt="banner-thumb" /></a>
<a data-slide-index="2" href="#"><img src="<?php echo base_url() ?>assets/images/icons/banner-thumb3.png" alt="banner-thumb" /></a>
</div>
</div><!-- /.banner-structure -->

<!-- ============== Start up section ============== -->

<!-- ============== Start up section ============== -->


<div class="start-up">
<div class="container">
<div class="row">
<div class="col-lg-7 col-md-7 col-sm-7 start-left">
<div class="title_number">How</div>
<h1>We are Best?<span class="all-level">Leading Expertise:</span></h1>
<p>Oracle's own experts providing thought leadership for every Oracle solution.</p>
<img src="<?php echo base_url() ?>assets/images/plant-image.png" class="left" alt="plant" />
</div><!-- /.start-left -->
<div class="col-lg-5 col-md-5 col-sm-5 start-right">
<a href="#" class="boost">
<h2>Oracle Consulting</h2>
<p>Highly qualified team of Paradigmsoft with expertise in all the Oracle E-Business Suite technologies ...</p>
</a>
<a href="#" class="filtering">
<h2>Web Design & Development</h2>
<p>Having worked with clients from wide range of industries, the expertise we gained in serving various ....</p>
</a>
<a href="#" class="solution">
<h2>Mobile Applications</h2>
<p>Our team has expertise in developing Mobile Applications with rich user interface. We consider constraints & contexts....</p>
</a>
<input type="button" class="more-service" value="more service"/>
</div><!-- /.start-right -->
</div><!-- /.row -->
</div><!-- /.container -->
</div><!-- /.start-up -->

<!-- ============== features section ============== --> 

<div class="features-section">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="title_number">03</div>
<h1>Features<span class="all-level">Some of our features</span></h1>
</div><!-- /.col-sm-12 -->
<div class="feature-box">
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 w100">
<a href="#" class="data">
<h2>Data Entry</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
</a>
</div><!-- /.col-lg-4 -->

<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 w100">
<a href="#" class="client">
<h2>Client management</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
</a>
</div><!-- /.col-lg-4 -->

<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 w100">
<a href="#" class="better">
<h2>Better Communication</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
</a>
</div><!-- /.col-lg-4 -->

<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 w100">
<a href="#" class="well-a">
<h2>Well analyzing team</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
</a>
</div><!-- /.col-lg-4 -->

<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 w100">
<a href="#" class="hour">
<h2>24 hour support</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
</a>
</div><!-- /.col-lg-4 -->

<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 w100">
<a href="#" class="keep">
<h2>Keeping business data</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
</a>
</div><!-- /.col-lg-4 -->
<div class="col-sm-12 center">
<input class="more-service" type="button" value="more service">
</div><!-- /.center -->
</div><!-- /.feature-box -->   
</div><!-- /.row -->
</div><!-- /.container -->
</div><!-- /.features-section -->

<!-- ============== service section ============== --> 
<div class="features-section">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="title_number">04</div>
<h1 class="paddingbot">Services offered</h1>
<br>
</div><!-- /.col-sm-12 -->

<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 w100">
<a href="<?php echo base_url() ?>Pages/service2" class="">
<img src="<?php echo base_url() ?>assets/images/features50.png" class="img-fluid" alt="banner" />
</a>
<section class="cpad">
<h2 class="margins">Oracle E-Business Suite</h2>
<br>
<p class="paddingbot">Whether your Business is Small or Large, Oracle ERP Implementation facilitates .</p>

</div>
</section>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 w100">
<a href="<?php echo base_url() ?>Pages/service2" class="">
<img src="<?php echo base_url() ?>assets/images/features51.png"  class="img-fluid" alt="banner" />
</a>
<section class="cpad">
<h2 class="margins">Oracle Fusion Applications</h2>
<br>
<p class="paddingbot">Oracle Fusion Applications is the next-generation application suite from Oracle..</p>
<br>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 w100">
<a href="<?php echo base_url() ?>Pages/service2" class="">
<img src="<?php echo base_url() ?>assets/images/features52.png"  class="img-fluid" alt="banner" />
</a>
<section class="cpad">
<h2>Oracle Fusion Middleware</h2>
<br>
<p class="paddingbot">If you’re looking to make your Oracle or multi-vendor IT environment more..</p>
<br>
</section>
</div>
</div>

</div>
</div>


<!-- ============== Team section ============== -->

<div class="team-section">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="title_number">05</div>
<h1 class="m-bottom">Team<span class="all-level">Meet our team</span></h1>
</div><!-- /.col-sm-12 -->
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 w100">
<div class="team-members">
<img src="<?php echo base_url() ?>assets/images/team1a.jpg" alt="team member" />
<div class="team-member-details">
<h2>Chowdary Kommalapati<span>Team leader</span></h2>
<div class="team-member-socials">
<a href="#"><i class="fa team-member-icons fa-facebook"></i></a>
<a href="#"><i class="fa team-member-icons fa-twitter"></i>
</a><a href="#"><i class="fa team-member-icons fa-google-plus"></i></a>
</div><!-- /.team-member-socials -->
</div><!-- /.team-member-details -->
</div><!-- /.team-members -->
</div><!-- /.col-lg-3 -->

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 w100">
<div class="team-members">
<img src="<?php echo base_url() ?>assets/images/team2a.jpg" alt="team member" />
<div class="team-member-details">
<h2>Anitha Reddy<span>Client Support</span></h2>
<div class="team-member-socials">
<a href="#"><i class="fa team-member-icons fa-facebook"></i></a>
<a href="#"><i class="fa team-member-icons fa-twitter"></i>
</a><a href="#"><i class="fa team-member-icons fa-google-plus"></i></a>
</div><!-- /.team-member-socials -->
</div><!-- /.team-member-details -->
</div><!-- /.team-members -->
</div><!-- /.col-lg-3 -->

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 w100">
<div class="team-members">
<img src="<?php echo base_url() ?>assets/images/team3a.jpg" alt="team member" />
<div class="team-member-details">
<h2>VidyaSagar A<span>Client Management</span></h2>
<div class="team-member-socials">
<a href="#"><i class="fa team-member-icons fa-facebook"></i></a>
<a href="#"><i class="fa team-member-icons fa-twitter"></i>
</a><a href="#"><i class="fa team-member-icons fa-google-plus"></i></a>
</div><!-- /.team-member-socials -->
</div><!-- /.team-member-details -->
</div><!-- /.team-members -->
</div><!-- /.col-lg-3 -->

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 w100">
<div class="team-members">
<img src="<?php echo base_url() ?>assets/images/team4a.jpg" alt="team member" />
<div class="team-member-details">
<h2>rajesh reddy<span>Communication</span></h2>
<div class="team-member-socials">
<a href="#"><i class="fa team-member-icons fa-facebook"></i></a>
<a href="#"><i class="fa team-member-icons fa-twitter"></i>
</a><a href="#"><i class="fa team-member-icons fa-google-plus"></i></a>
</div><!-- /.team-member-socials -->
</div><!-- /.team-member-details -->
</div><!-- /.team-members -->
</div><!-- /.col-lg-3 -->
</div><!-- /.row -->
</div><!-- /.container -->
</div><!-- /.team-section -->

<!-- ============== Month Section ============== -->

<div class="month">
<div class="container">
<div class="row">
<h2>One Month <span>Free</span> Service</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
<div class="col-sm-6 col-xs-6 w100"><input type="button" class="sign-up" value="sign up" /></div><!-- /.col-sm-6 -->
<div class="col-sm-6 col-xs-6 w100"><input type="button" class="contact-bt" value="contact us" /></div><!-- /.col-sm-6 -->
</div><!-- /.row -->
</div><!-- /.container -->
</div><!-- /.month -->
